<?php
// foreach loop

$arr = ['apple','kiwi','ball','cat','mango','melon'];
foreach($arr as $a){
    echo $a."<br>";
}

echo "<br><br>";
// or
foreach($arr as $i => $value){
    echo "$i $value<br>";
}

?>