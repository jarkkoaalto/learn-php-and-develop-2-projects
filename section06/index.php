<?php
include('header.php');
include('db.php');
?>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">  
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>

<?php
$sql ="SELECT * FROM clients";
$results = mysqli_query($db, $sql);
?>


<div class="content">
    <h2>Your client information</h2>
<div class="float-right">
    Total clients: <?php echo mysqli_num_rows($results); ?>
</div>
<table id="table_id" class="display">
    <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Address</th>
            <th>Description</th>
            <th>Date</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($results as $result)
    {
        ?>
        <tr>
            <td><?php echo $result['name'];?></td>
            <td><?php echo $result['email'];?></td>
            <td><?php echo $result['phone'];?></td>
            <td><?php echo $result['address'];?></td>
            <td><?php echo $result['description'];?></td>
            <td><?php echo $result['created'];?></td>
            <td>
                <button class="btn btn-success btn-sm">View</button>
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal<?php echo $result['id']; ?>">
                        Edit
                </button>
                <button class="btn btn-danger btn-sm">Delete</button>
            </td>
            
        </tr>
        <!-- Modal -->
     
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel<?php echo $result['id']; ?>" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
   
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title<?php echo $result['id']; ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="update.php" method="post">
      <div class="modal-body">
      <div class="form-group">
        <label for="name">Name</label>
        <input type="text" name="name" class="from-control">
      </div>
      <div class="form-group">
        <label for="name">Email</label>
        <input type="email" name="email" class="from-control">
      </div>
      <div class="form-group">
        <label for="name">Phone</label>
        <input type="text" name="phone" class="from-control">
      </div>
      <div class="form-group">
        <label for="name">Address</label>
        <input type="text" name="address" class="from-control">
      </div>
      <div class="form-group">
        <label for="name">Description</label>
        <textarea name="description" class="form-control"></textarea>
      </div>
      </div>
      <div class="modal-footer">
        <button  class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
    <?php } ?> 
    </tbody>
</table>
</div>
</form>
<script type="text/javascript">
$(document).ready( function () {
    $('#table_id').DataTable();
} );

</script>